/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionarioexamen;

import cl.dto.PalabraDTO;
import com.mycompany.diccionarioexamen.dao.PalabrasJpaController;
import com.mycompany.diccionarioexamen.entity.Palabras;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pablo
 */
@Path("diccionario")
public class Diccionarioapi {
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("idbuscar") String idbuscar){
        
         Client client = ClientBuilder.newClient();
        
         WebTarget myResource = client.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/" + idbuscar);
         PalabraDTO palabradto = myResource.request(MediaType.APPLICATION_JSON).header("app_id", "214565f2").header("app_key", "15f9d9b374c0cacafc7f9254746f5f12").get(PalabraDTO.class);
         
         Palabras pal = new Palabras();
         
         pal.setPalabra(palabradto.getWord());
         Date fecha =new Date();         
         pal.setFecha(fecha.toString());
           
        
            List<Palabras> lista=new ArrayList<Palabras>();
    
   lista.add(pal);
  
    return Response.ok(200).entity(lista).build();            
                
                
     
}
    
    
}
